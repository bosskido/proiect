/*When document is ready*/
var baseUrl = "http://localhost:8000";

var bara=document.getElementById('div4');

/*Declare global functions*/
var apiSuccess = function(data){
		return data.code == 200;
	},
	logFailure = function(data){
		if(data && data.message != undefined)
			alert(data.message);
	},
	redirectTo = function(page){
		if(page != undefined)
			window.location.href = './' + page;
	},
		loadLogat=function(){
    $(document).ready(function(){
    	bara.style.display='inline-block';
        $.ajax({url: "ApplicationLogic/Views/logat.html", success: function(result){
            $("#div3").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/meniu7.html", success: function(result){
            $("#div2").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/bara4.html", success: function(result){
            $("#div4").html(result);
        }});
    });
},
		loadOk=function(){
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/copii.html", success: function(result){
            $("#div3").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/meniu7.html", success: function(result){
            $("#div2").html(result);
        }});
    });
},
loadLogin=function(){
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/login.html", success: function(result){
            $("#div3").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/meniu3.html", success: function(result){
            $("#div2").html(result);
        }});
    });
},
loadSignup=function(){
    $(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/signup.html", success: function(result){
            $("#div3").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/meniu4.html", success: function(result){
            $("#div2").html(result);
        }});
    });
},
loadCreatcont=function(){
	$(document).ready(function(){
        $.ajax({url: "ApplicationLogic/Views/creat.html", success: function(result){
            $("#div3").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/meniu4.html", success: function(result){
            $("#div2").html(result);
        }});
    });
},
loadLogout=function()
{
    $(document).ready(function(){
    	bara.style.display='none';
        /*$.ajax({url: "bara.html", success: function(result){
            $("#div4").html(result);
        }});*/
        $.ajax({url: "ApplicationLogic/Views/delogat.html", success: function(result){
            $("#div3").html(result);
        }});
        $.ajax({url: "ApplicationLogic/Views/meniu5.html", success: function(result){
            $("#div2").html(result);
        }});
    });
};

var loadChildren = function() {
	socket =  io('http://localhost:8000');

	var data = {};
	data.username = localStorage['user'];
	$.ajax({
		type: "POST",
		url: baseUrl + "/getChildren",
		data: data
	})
	.done(function( response ) {
		/*If everything is ok*/
		if(apiSuccess(response))
		{
			var listaCopii = "";

			for (var child in response.data){
				var childId = child;
				childId = childId.replace(" ", "");

				listaCopii += "<li id='"+ childId +"id'>" + child + "<ul>";

				listaCopii += "<li class='no-map'><button class='showMap' data-id='"+childId+"'>Show Map</button></li>";

				/*Show distance, real time*/
				listaCopii += "<li class='dist'>0m</li>";
				listaCopii += "<li class='status'>Situatie: Copilul sa pe loc</li>";

				listaCopii += "</ul> </li>";



				socket.childId = childId;

				socket.emit("reg", {
					username: data.username,
					child: child
				});

			}
			$(".lista-copii").html(listaCopii);

			$(".showMap").click(function(){
				var childId = $(this).attr("data-id");
				$("#map").attr('class', childId);
			});

			socket.on('move', function (data) {
				var id = data.child.replace(" ", "");

				/*Update the distance*/
				$("#" + id + "id").find(".dist").text("Distanta parcursa: " + data.moved_distance*10 + "m");

				if(data.moved_distance >= data.map.original.perimeter)
				{
					$("#" + id + "id").find(".status").html("Status: Pericol, copilul s-a indepartat prea mult!");
				}
				else
				{
					$("#" + id + "id").find(".status").html("Status: Copilul este in siguranta, dar se indeparteaza de zona")

				}

				if($("#map").hasClass(id))
				{
					renderMap("map", data.map);
				}
			});
		}
		else
		{
			/* Alert the user that we have a problem */
			logFailure(response);
		}
	});
}

facebookSignup = function(response){
	var data = {};
	data.username = response.name;
	data.password = "facebook";

	$.ajax({
		type: "POST",
		url: baseUrl + "/signup",
		data: data
	})
	.done(function( response ) {
		/*If everything is ok*/
		if(apiSuccess(response))
		{
			/* Redirect to main page */
			loadLogat();
		}
		else
		{
			/* Alert the user that we have a problem */
			redirectTo('../../ApplicationLogic/Views/copii.html');
		}
	});
}


$(".showMap").click(function(){
	var childId = $(this).attr("data-id");
	$("#map").attr('class', childId);
});
$(document).ready(function(){


	if(!$("#onChildrenPage").length)
	{
		if(localStorage["user"])
		{
			/*It means one user is logged in so we send him the children and status for each one*/
				redirectTo('ApplicationLogic/Views/copii.html');
			}
	}
	else
	{
		localStorage['user'] = localStorage['user'];
		$("#onChildrenPage .mesaj").text("Bine ai revenit, " + localStorage['user'] + "!");
		loadChildren();
	}

	$(".signup-submit").click(function(e){
		/*Prevent form submission*/
		e.preventDefault();
		var data = {};

		/*This flag will be used for validation*/
		var canProceed = true;

		/*Get username and passwords and prepare for ajax request*/
		data.username = $("#username").val();
		data.password = $("#password").val();
		data.repassword = $("#re-password").val();

		if(data.username == "" || data.password == "" || data.repassword == "")
		{
			alert("Introduceti toate datele!")
			canProceed = false;
		}else
		/*Validate passwords*/
		if(data.password != data.repassword)
		{
			alert("Parolele nu se potrivesc!");
			canProceed = false;
		}else{
			if(data.username.length<8){
				alert("Username-ul trebuie sa aiba minim 8 caratere!");
				canProceed = false;
			}else
			{
				if(data.password.length<8){
					alert("Parola trebuie sa aiba minim 8 caratere!");
					canProceed = false;
				}
			}
		}
		
		/*If our flag is true, we can proceed to the ajax call*/
		if(canProceed)
		{		

			$.ajax({
				type: "POST",
				url: baseUrl + "/signup",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					/* Redirect to main page */
					//alert("ssss");

					loadCreatcont();
				}
				else
				{
					/* Alert the user that we have a problem */
					//alert("lll");
					logFailure(response);
				}
			});
		}

	});
	
	$(".login-submit").click(function(e){
		/*Prevent form submission*/
		e.preventDefault();
		
		var data = {};

		/*This flag will be used for validation*/
		var canProceed = true;

		/*Get username and passwords and prepare for ajax request*/
		data.username = $("#username").val();
		data.password = $("#password").val();

		if(data.password == "" || data.username == "")
		{
			alert("Introduceti si username-ul si parola!");
			canProceed = false;
		}

		if(canProceed)
		{
			$.ajax({
				type: "POST",
				url: baseUrl + "/login",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					/* Redirect to main page */
					localStorage["user"] = data.username;
					loadLogat();
				}
				else
				{
					/* Alert the user that we have a problem */
					logFailure(response);
				}
			});
		}
	});

	$("#logout").click(function(){
		localStorage.removeItem('user');
		redirectTo('../../index.html');
		});

	$("#ok").click(function(){
		//loadOk();
		redirectTo('ApplicationLogic/Views/copii.html');
	});

	$(".addChild").click(function(e) {
		e.preventDefault();

		$(".cont.lista").hide();
		$(".cont.adauga-copil").show();
	});

	
$("#add-child").click(function(e){
		e.preventDefault();

		var data = {};
		var canProceed = true;

		data.name = $("#nume").val();
		data.lat = $("#lat").val();
		data.lng = $("#lng").val();
		data.perimetru = $("#perimetru").val();
		data.username = localStorage['user'];

		if(data.name == "" || data.lat == "" || data.lng == "" || data.perimetru == "")
		{
			alert("Completati toate campurile!");
			canProceed = false;	
		}

		if(canProceed)
		{
			$.ajax({
				type: "POST",
				url: baseUrl + "/add-child",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					/* Redirect to main page */
					$(".cont.lista").show();
					$(".cont.adauga-copil").hide();

					loadChildren();
				}
				else
				{
					/* Alert the user that we have a problem */
					//logFailure(response);
					alert("Exista deja un copil cu acest nume!");
				}
			});
		}

	});

$("#delete-child").click(function(e){

		e.preventDefault();

		var data = {};
		var canProceed = true;

		data.name = $("#copilNume").val();
		data.username = localStorage['user'];

		if(data.name == "")
		{
			alert("Introduceti numele copilului!");
			canProceed = false;	
		}

		if(canProceed)
		{
			$.ajax({
				type: "POST",
				url: baseUrl + "/delete-child",
				data: data
			})
			.done(function( response ) {
				/*If everything is ok*/
				if(apiSuccess(response))
				{
					/* Redirect to main page */
					$(".cont.lista").show();
					$(".cont.adauga-copil").hide();

					loadChildren();
				}
				else
				{
					/* Alert the user that we have a problem */
					logFailure(response);
				}
			});
		}


	});

	
	
})
